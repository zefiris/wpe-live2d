// background.js
const canvas = document.getElementById('background');

canvas.width = CANVAS_SIZE.width;
canvas.height = CANVAS_SIZE.height;

let bgImg;

function render(t) {
  const ctx = canvas.getContext('2d');

  ctx.drawImage(bgImg, 0, 0, CANVAS_SIZE.width, CANVAS_SIZE.height);
}

function preload() {
  bgImg = new Image();
  bgImg.src = 'img/bg.jpg';

  return new Promise((resolve, reject) => {
    bgImg.onload = () => {
      resolve();
    }
  });
};

function init() {
  Promise.resolve()
    .then(() => {
      return preload();
    })
    .then(() => {
      render();
    });
}

init();
