const idle = (l2d) => () => {
  l2d.startRandomMotion('idle', 1);
  requestAnimationFrame(idle(l2d));
};

loadLive2d({
  canvas: 'live2d',
  baseUrl: 'models/c238_02',
  model: 'models/c238_02/MOC.c238_02.json',
  debug: {
    DEBUG_LOG: true,
  },
  layout: {
    center_x: -0.3,
    center_y: -0.1,
    width: 0.9,
    height: 0.9,
    // x: "",
    // y: "",
    // center_x: "",
    // center_y: "",
    // top: "",
    // bottom: "",
    // left: "",
    // right: ""
  },
  ...CANVAS_SIZE,
  initModelCallback(l2d) {
    idle(l2d)();
  },
});
