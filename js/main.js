const ASPECT_RATIO = 16/9;
const CANVAS_SIZE = getCanvasDimension();

function getCanvasDimension() {
  if (window.innerWidth > window.innerHeight * ASPECT_RATIO) {
    return {
      width: window.innerWidth,
      height: window.innerWidth / ASPECT_RATIO,
    };
  } else {
    return {
      width: window.innerHeight * ASPECT_RATIO,
      height: window.innerHeight,
    };
  }
}

function getScaledX(x) {
  return x * CANVAS_SIZE.width;
}

function getScaledY(y) {
  return y * CANVAS_SIZE.height;
}
